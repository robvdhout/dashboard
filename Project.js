import client from "./client.js";

export default class Project {
    constructor({pid}, button) {
        this.pid = pid;
        this.button = button;
    }

    async getMergeRequestsToAcceptance() {
        return client.get(`projects/${this.pid}/merge_requests/?target_branch=acceptance&state=opened`);
    }
}