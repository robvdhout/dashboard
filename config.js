export const projects = {
    "MijnNVSchade": {
        pid: 'portals%2Fdrupal%2Fmijnnvschade',
        reviewer_ids: [
            3, // Mitch
            4, // Kevin
            5, // Quincy
            31 // Rik
        ],
    },
    "Verzuim": {
        pid: 'portals%2Fdrupal%2Fverzuimnavigator',
        reviewer_ids: [],
    },
};