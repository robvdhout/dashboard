import {displayOpenMergeRequests} from "./MergeRequests/open.js";
import handleTestPipeline from "./Pipelines/tst.js";
import handleAcceptancePipeline from "./Pipelines/acc.js";
import reload from "./reload.js";
import {displayReviewRequests} from "./MergeRequests/review.js";

const promises = [];

promises.push(displayOpenMergeRequests(document.getElementById('open_merge_requests')));
promises.push(displayReviewRequests(document.getElementById('review_requests')));

for (let button of document.getElementsByClassName('TST')) {
    promises.push(handleTestPipeline(button));
}

for (let button of document.getElementsByClassName('ACC')) {
    promises.push(handleAcceptancePipeline(button));
}

Promise.all(promises).then(() => {
    reload.trigger();
});

