class Client {

    server = 'https://git.indicia.nl/api/v4/';

    async connect() {
        this.secret = this.getSecret();
        this.user = await this.get(`user`);
    }

    getSecret() {
        const key = 'personal_access_token';
        let item = localStorage.getItem(key);

        if (!item) {

            item = prompt('Please fill in your personal access token');
            localStorage.setItem(key, item);
        }

        return item;
    }

    get(path) {
        return this.request('get', path);
    }

    async request(method, path, body, headers = {}) {

        headers = {
            ...headers,
            'PRIVATE-TOKEN': this.secret
        }

        let response = await fetch(
            `${this.server}${path}`,
            {
                method,
                body,
                headers
            });
        return response.json();
    }

    send(method, path, data) {
        return this.request(method, path, JSON.stringify(data), {'content-type': 'application/json'});
    }

    put(path, data) {
        return this.send('put', path, data);
    }

    post(path, data) {
        return this.send('post', path, data);
    }
}

const client = new Client();

await client.connect();
export default client;