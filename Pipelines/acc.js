import Project from "../Project.js";
import client from "../client.js";
import {projects} from "../config.js";
import reload from "../reload.js";

export default async function handleAcceptancePipeline (button) {

    const config = projects[button.dataset.project];
    const pid = config.pid;
    const project = new Project({pid}, button);

    const acceptance_merge_requests = await project.getMergeRequestsToAcceptance();

    if (acceptance_merge_requests.length === 0) {

        const pipelines = await client.get(`projects/${pid}/pipelines/?scope=branches&ref=acceptance`);
        const pipeline = pipelines[0];

        if (pipeline.status === 'success') {

            const comparison = await client.get(`projects/${pid}/repository/compare?from=acceptance&to=master`);

            if (comparison.diffs.length > 0) {


                button.innerHTML = `create MR (last release by ${comparison.commit.committer_name} at ${pipeline.updated_at})`;
                button.onclick = async () => {
                    await client.post(`projects/${pid}/merge_requests`, {
                        source_branch: 'master',
                        target_branch: 'acceptance',
                        title: 'Master',
                        assignee_id: client.user.id,
                        reviewer_ids: config.reviewer_ids,
                    });

                    reload.delay = 0;
                }
            } else {
                button.innerHTML = `last release at ${pipeline.updated_at}`;
                button.onclick = () => {
                    window.open(comparison.web_url, `${pid}-acc`);
                    reload.delay = 0;
                }
            }

        } else {
            if (pipeline.status === 'manual') {
                button.innerHTML = 'finish Pipeline';
                button.onclick = async () => {

                    const jobs = await client.get(`projects/${pid}/pipelines/${pipeline.id}/jobs`);

                    for (const job of jobs) {
                        if (job.status === 'manual') {
                            await client.post(`projects/${pid}/jobs/${job.id}/play`);
                        }
                    }
                    reload.delay = 0;
                }
            } else {
                button.innerHTML = `${pipeline.status}: view Pipeline`;
                button.onclick = () => {
                    window.open(pipeline.web_url, `${pid}-acc`);
                    reload.delay = 0;
                }
                reload.delay = 5;
            }
        }
    } else {
        const merge_request = acceptance_merge_requests[0];

        const {approved} = await client.get(`projects/${pid}/merge_requests/${merge_request.iid}/approvals`);

        if (approved) {
            button.innerHTML = 'merge MR';
            button.onclick = async () => {
                await client.put(`projects/${pid}/merge_requests/${merge_request.iid}/merge`);
                reload.delay = 0;
            }
        } else {
            button.innerHTML = 'view MR';
            button.onclick = () => {
                window.open(merge_request.web_url, `${pid}-acc`);
                reload.delay = 0;
            }
        }
    }
}