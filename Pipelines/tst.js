import client from "../client.js";
import {projects} from "../config.js";
import reload from "../reload.js";

export default async function handleTestPipeline(button) {
    const config = projects[button.dataset.project];
    const pid = config.pid;

    const pipelines = await client.get(`projects/${pid}/pipelines/?scope=branches&ref=master`);
    const pipeline = pipelines[0];

    if (pipeline.status === 'manual') {
        button.innerHTML = 'finish Pipeline';
        button.onclick = async () => {

            const jobs = await client.get(`projects/${pid}/pipelines/${pipeline.id}/jobs`);

            for (const job of jobs) {
                if (job.status === 'manual') {
                    await client.post(`projects/${pid}/jobs/${job.id}/play`);
                }
            }
            reload.delay = 0;
        }
    } else {
        button.innerHTML = `${pipeline.status}: view Pipeline`;
        button.onclick = () => {
            window.open(pipeline.web_url, `${pid}-tst`);
            reload.delay = 0;
        }
    }
}