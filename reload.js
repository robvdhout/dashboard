export default class {

    static timeout = 60 * 1000;
    static triggered = false;
    static enabled = true;
    static timeoutID;

    static trigger() {
        if (!this.enabled) {
            return;
        }
        this.clear();
        this.timeoutID = setTimeout(() => window.location.reload(), this.timeout);
        this.triggered = true;
    }

    static set delay(sec) {
        this.timeout = Math.min(sec * 1000, this.timeout);

        if (this.triggered) {
            this.trigger();
        }
    }

    static disable() {
        this.enabled = false;
        this.clear();
    }

    static clear(){
        clearTimeout(this.timeoutID);
    }
};