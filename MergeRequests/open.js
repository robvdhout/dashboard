import display from "../display.js";
import client from "../client.js";
import reload from "../reload.js";
import highlight from "../highlight.js";

function createMarkAsReadyButton(merge_request, handler) {
    const ref = {};
    return {
        type: 'button',
        text: 'mark as ready',
        ref,
        action: _ => {

            client.put(`projects/${merge_request.project_id}/merge_requests/${merge_request.iid}`, {
                title: merge_request.title.substring(7),
            }).then(merge_request => {
                handler.update('title', merge_request.title);
                ref.element.remove();
            })
        },
    };
}

class Handler {

    fieldElements = {};

    register(field, element) {
        this.fieldElements[field] = this.fieldElements[field] || [];
        this.fieldElements[field].push(element);
    }

    update(field, text) {
        this.fieldElements[field].forEach(element => {
            element.innerText = text;
        });
    }
}

export const displayOpenMergeRequests = async function (openMergeRequestsUL) {

    const open_merge_request_display = new display(
        openMergeRequestsUL
    );
    const merge_requests = await client.get(`merge_requests/?target_branch=master&state=opened`);

    let toDisplay = [];

    const handler = new Handler();

    for (const merge_request of merge_requests) {

        let details = [
            {
                field: 'state',
                type: 'text',
                text: 'state: ' + merge_request.state,
            }
        ];

        if (merge_request.draft) {
            details.push(createMarkAsReadyButton(merge_request, handler))
        }

        const reviewers = await client.get(`projects/${merge_request.project_id}/merge_requests/${merge_request.iid}/reviewers`);

        details.push(reviewers.map(({state, user: {name, web_url}}) => {

            if (state !== 'unreviewed') {
                reload.disable();
                highlight();
            }

            return {
                field: 'title',
                text: `${name} ${state}`,
                type: 'link',
                url: web_url,
            }
        }));

        toDisplay.push(
            {
                field: 'title',
                text: merge_request.title,
                type: 'link',
                url: merge_request.web_url,
            },
            details
        );
    }

    open_merge_request_display.display(toDisplay, handler);
};