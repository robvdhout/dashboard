import display from "../display.js";
import client from "../client.js";
import highlight from "../highlight.js";

class Handler {

    fieldElements = {};

    register(field, element) {
        this.fieldElements[field] = this.fieldElements[field] || [];
        this.fieldElements[field].push(element);
    }

    update(field, text) {
        this.fieldElements[field].forEach(element => {
            element.innerText = text;
        });
    }
}

export const displayReviewRequests = async function (revieweRequestsUL) {

    const review_requests_display = new display(
        revieweRequestsUL
    );
    const review_requests = await client.get(`merge_requests/?scope=all&state=opened&reviewer_id=${client.user.id}`);

    let toDisplay = [];

    const handler = new Handler();

    for (const merge_request of review_requests) {

        const reviewers = await client.get(`projects/${merge_request.project_id}/merge_requests/${merge_request.iid}/reviewers`);

        toDisplay.push(
            {
                field: 'title',
                text: merge_request.title,
                type: 'link',
                url: merge_request.web_url,
            },
        );
        toDisplay.push(reviewers.map(({state, user: {id, name, web_url}}) => {

            if (state === 'unreviewed' && id === client.user.id) {
                highlight();
            }

            return {
                field: 'title',
                text: `${name} ${state}`,
                type: 'link',
                url: web_url,
            }
        }));

    }

    review_requests_display.display(toDisplay, handler);
};