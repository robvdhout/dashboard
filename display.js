import Controller from "./controller.js";


export default class Display {
    constructor(list) {
        this.list = list;
    }

    display(elements, handler) {
        for (let elem of elements) {
            const li = document.createElement('li');

            if (Array.isArray(elem)) {
                const ul = document.createElement('ul');
                (new Display(ul)).display(elem, handler);
                this.list.append(ul);
            }
            else if (elem === Object(elem)) {
                let node = this.render(elem);
                handler.register(elem.field, node);
                li.append(node);
                this.list.append(li);
            }
            else {
                li.innerHTML = elem;
                this.list.append(li);
            }
        }
    }

    render(elem) {
        let element;
        switch (elem.type) {
            case 'text':
                element = document.createElement('span');
                element.innerHTML = elem.text;
                return element;
            case 'link':
                element = document.createElement('a');
                element.href = elem.url;
                element.innerHTML = elem.text;
                element.target = '_blank';
                return element;
            case 'button':
                element = document.createElement('button');
                element.onclick = elem.action;
                element.innerHTML = elem.text;
                elem.ref.element = element;
                return element;
        }
    }
}